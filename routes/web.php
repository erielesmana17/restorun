<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');

Route::get('/login', 'LoginController@masuk');

Route::get('/register', 'RegisterController@daftar');

Route::post('/welcome', 'DashboardController@kirim');

Route::group([
    'prefix' => 'resto',
    'name' => 'resto.'
],function(){
    //CRUD Resto
    Route::get('/', 'RestoController@index')->name('index');
    Route::get('/create', 'RestoController@create')->name('create');
    Route::post('/', 'RestoController@store')->name('store');
    Route::get('/{resto}', 'RestoController@show')->name('show');
    Route::get('/{resto}/edit', 'RestoController@edit')->name('edit');
    Route::put('/{resto}', 'RestoController@update')->name('update');
    Route::delete('/{resto}', 'RestoController@destroy')->name('destroy');
});
    

Route::group([
    'prefix' => 'menu',
    'name' => 'menu.'
],function(){
    //CRUD Menu
    Route::get('/{resto}', 'MenuController@index')->name('menu.index');
    Route::get('/{resto}/create', 'MenuController@create')->name('create');
    Route::post('/{resto}', 'MenuController@store')->name('store');
    Route::get('/{resto}/{menu}', 'MenuController@show')->name('show');
    Route::get('/{resto}/{menu}/edit', 'MenuController@edit')->name('edit');
    Route::put('/{resto}/{menu}', 'MenuController@update')->name('update');
    Route::delete('/{resto}/{menu}', 'MenuController@destroy')->name('destroy');

});

// Update Profil
Route::resource('profile', 'ProfileController')->only([
    'index','update'
]);

Route::group([
    'prefix' => 'rating',
    'name' => 'rating.'
],function(){
//CRUD rating
Route::post('/', 'RatingController@store')->name('rating');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
