<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "menu";
    protected $guarded = [];

    public function resto()
    {
        return $this->belongsTo('App\Resto');
    }

    
}
