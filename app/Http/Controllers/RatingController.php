<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Rating;

class RatingController extends Controller
{
    
    public function store(Request $request)
    {
        $request->validate([
            'isi'=>'required'
        ]);
        $rating = new Rating;

        $rating->isi = $request->isi;
        $rating->user_id = Auth::id();
        $rating->menu_id = $request->menu_id;

        $rating->save();


        return redirect()->back();
    }
}
