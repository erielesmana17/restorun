<?php

namespace App\Http\Controllers;
use App\Menu;
use App\Resto;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class MenuController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Resto $resto)
    {
        $menu = $resto->menu;
        return view('menu.index', compact('menu','resto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Resto $resto)
    {
        return view('menu.create', compact('resto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Resto $resto)
    {
        $request->validate([
            'nama'=>'required',
            'keterangan' => 'required',
            'harga'=>'required'
        ]);
        Menu::create([
            'nama'=> $request ->nama,
            'keterangan'=> $request ->keterangan,
            'harga'=> $request ->harga,
            'resto_id' => $resto->id
        ]);

        Alert::success('Tambah menu berhasil', 'menu berhasil ditambahkan!');
        return redirect()->route('menu.index', compact('resto'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Resto $resto, Menu $menu)
    {
        
        return view('menu.show', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Resto $resto, Menu $menu)
    {
        
        return view('menu.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resto $resto, Menu $menu)
    {
        $request->validate([
            'nama' => 'required|unique:menu',
            'keterangan' => 'required',
            'harga'=>'required'
        ]);

        
        $menu->nama = $request->nama;
        $menu->keterangan = $request->keterangan;
        $menu->harga = $request->harga;
        $menu->resto_id = $request->resto->id;
        $menu->update();
        
        Alert::success('Update menu berhasil', 'menu berhasil diupdate!');
        return redirect()->route('menu.index', compact('resto'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resto $resto, Menu $menu)
    {
        
        $menu->delete();
        return redirect('/menu');
    }

    public function dataTable(){
        return view('menu.index');
        
    }
}
