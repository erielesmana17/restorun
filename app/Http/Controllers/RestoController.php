<?php

namespace App\Http\Controllers;
use App\Resto;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class RestoController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resto = Resto::all();
        return view('resto.index', compact('resto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('resto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'alamat' => 'required',
            'kontak'=>'required'
        ]);

        $resto = new Resto;

        $resto->nama = $request->nama;
        $resto->alamat = $request->alamat;
        $resto->kontak = $request->kontak;

        $resto->save();

        Alert::success('Tambah resto berhasil', 'Tambah resto berhasil');

        return redirect('/resto');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Resto $resto)
    {
        
        return view('resto.show', compact('resto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Resto $resto)
    {
        
        return view('resto.edit', compact('resto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resto $resto)
    {
        $request->validate([
            'nama' => 'required|unique:resto',
            'alamat' => 'required',
            'kontak'=>'required'
        ]);


        $resto->nama = $request->nama;
        $resto->alamat = $request->alamat;
        $resto->kontak = $request->kontak;
        $resto->update();
        Alert::success('Update resto berhasil', 'Update resto berhasil');
        return redirect('/resto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resto $resto)
    {
        
        $resto->delete();
        return redirect('/resto');
    }
}
