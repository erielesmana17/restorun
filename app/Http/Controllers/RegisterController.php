<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


class RegisterController extends Controller
{
    public function daftar(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        $username = $request['username'];
        $email = $request['email'];
        $password = $request['password'];
        $umur = $request['umur'];
        $alamat = $request['alamat'];
        $bio = $request['bio'];

        Alert::success('Pendaftaran berhasil', 'Pendaftaran berhasil');
        
        return view('halaman.index', compact('username','email','umur','alamat','bio'));
    }
}
