<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resto extends Model
{
    protected $table = 'resto';
    protected $fillable = ['nama','alamat','kontak'];

    public function menu()
    {
        return $this->hasMany('App\Menu');
    }
}
