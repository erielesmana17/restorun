
@extends('layout.mastera')

@section('judul')
Detail Menu
@endsection
@section('content')
<h2>{{$menu->nama}}</h2>
<br>
<p>{{$menu->keterangan}}</p>
<p> Harga Rp {{$menu->harga}}</p>

<h1>Rating</h1>
@foreach ($menu->rating as $item)
<div class="card">
    <div class="card-body">
        <small><b>{{$item->user->name}}</b></small>
        <p class="card-text">{{$item->isi}}</p>

    </div>
</div>

    
@endforeach
<form action="/rating" method="POST" class="my-3">
    @csrf
    
    <div class="form-group">
      <label >Komentar</label>
      <input type="hidden" name="menu_id" value="{{$menu->id}}" id="">
      <textarea  name="isi" class="form-control"></textarea>
    </div>
    @error('isi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

<a href="/resto/" class="btn btn-info btn-sm">kembali</a>
    
@endsection
    

