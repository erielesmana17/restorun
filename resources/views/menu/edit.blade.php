
@extends('layout.mastera')

@section('judul')
Detail Menu
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/hfiufwcv5xwzdo6omwqcgc9iyvs9istkajdjlrixqbgd53xb/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
@endpush
@section('content')
<div>
    <h2>Update Menu {{$menu->id}}</h2>
    <form action="/menu/{{ $menu->resto_id }}/{{ $menu->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="">Nama Menu</label>
            <input type="text" class="form-control" name="nama" value="{{$menu->nama}}">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="">Keterangan</label>
            <input type="text" class="form-control" name="keterangan"  value="{{$menu->keterangan}}">
            @error('keterangan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="">Harga</label>
            <input type="text" class="form-control" name="harga"  value="{{$menu->harga}}">
            @error('harga')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>

    
@endsection
    

