@extends('layout.mastera')

@section('judul')

@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/hfiufwcv5xwzdo6omwqcgc9iyvs9istkajdjlrixqbgd53xb/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
@endpush
@section('content')

<h1>Restoran: {{$resto->nama}}</h1> 
<br>

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Menu</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Harga(Rp)</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
                @forelse ($menu as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{!!$value->keterangan!!}</td>
                        <td>{{$value->harga}}</td>
                        <td>
                            <a href="/menu/{{ $resto->id }}/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/resto" class="btn btn-success">Resto</a>
                            <a href="/menu/{{ $resto->id }}/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/menu/{{ $resto->id }}/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    <a href="/menu/{{ $resto->id }}/create" class="btn btn-primary mb-3">Tambah Menu</a>
@endsection
    

