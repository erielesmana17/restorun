@extends('layout.mastera')

@section('judul')
Tambah Menu
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/hfiufwcv5xwzdo6omwqcgc9iyvs9istkajdjlrixqbgd53xb/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
@endpush
@section('content')

<form action= "/menu/{{ $resto->id }}" method = "POST">
    @csrf
    <div class="form-group">
      <label >Nama </label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label >Keterangan</label>
      <textarea type="text" name="keterangan" class="form-control"></textarea>
    </div>
    @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >Harga</label>
        <input type="number" name="harga" class="form-control">
      </div>
      @error('harga')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
    

