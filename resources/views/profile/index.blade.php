@extends('layout.mastera')

@section('judul')
Halaman Profile
@endsection

@section('content')

<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Nama User</label>
        <input type="text"  value="{{$profile->user->name}}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>Email User</label>
        <input type="text" value="{{$profile->user->email}}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>Umur</label>
        <input type="integer" name="umur" value="{{$profile->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror

    <div class="form-group">
        <label>Biodata</label>
        <textarea type="text" name="bio" class="form-control">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror   

    <div class="form-group">
        <label>Alamat</label>
        <textarea type="text" name="alamat" class="form-control">{{$profile->alamat}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror

    <button type="submit" class="bten btn-primary">Update</button>
</form>

@endsection