<section class="recipe_section layout_padding-top">
    <div class="container">
      <div class="heading_container heading_center">
        <h2>
          Menu Populer Saat ini!
        </h2>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-4 mx-auto">
          <div class="box">
            <div class="img-box">
              <img src="{{asset('admin/images/r1.jpg')}}" class="box-img" alt="">
            </div>
            <div class="detail-box">
              <h4>
                Menu Sarapan
              </h4>
              <a href="">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 mx-auto">
          <div class="box">
            <div class="img-box">
              <img src="{{asset('admin/images/r2.jpg')}}" class="box-img" alt="">
            </div>
            <div class="detail-box">
              <h4>
                Menu Makan Siang
              </h4>
              <a href="">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 mx-auto">
          <div class="box">
            <div class="img-box">
              <img src="{{asset('admin/images/r3.jpg')}}" class="box-img" alt="">
            </div>
            <div class="detail-box">
              <h4>
                Menu Makan Malam
              </h4>
              <a href="">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </section>
