<header class="header_section">
    <div class="container-fluid">
      <nav class="navbar navbar-expand-lg custom_nav-container">
        <a class="navbar-brand" href="/">
          <span>
            RestoRun
          </span>
        </a>
        <div class="" id="">
          <div class="User_option">
            @guest
            <a href="login">
              <i class="fa fa-user" aria-hidden="true"></i>
              <span>Login</span>
            </a>
            @endguest

            @auth

            <form class="form-inline ">
              <a href="/profile" class="nav-link">User: {{Auth::user()->name}}</a>
            </form>

            <a href="/">
              <i class="fa fa-home" aria-hidden="true"></i>
              <span>home</span>
            </a>
            @endauth

            {{-- <form class="form-inline ">
              <input type="search" placeholder="Search" />
              <button class="btn  nav_search-btn" type="submit">
                <i class="fa fa-search" aria-hidden="true"></i>
              </button>
            </form> --}}

          </div>
          <div class="custom_menu-btn">
            <button onclick="openNav()">
              <img src="{{asset('admin/images/menu.png')}}" alt="">
            </button>
          </div>
          <div id="myNav" class="overlay">
            <div class="overlay-content">
              <a href="/">Home</a>
              <a href="/">About</a>
              <a href="/">Blog</a>
              
              @guest
              <a href="/login">Login</a>
              @endguest
              
              @auth
              <a href="resto">Daftar Resto</a>
              <a href="/">Testimonial</a>
              <a class="nav-link bg-danger" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">

                  <p>Logout</p>
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
              @endauth
              
            </div>
          </div>
        </div>
      </nav>
    </div>
  </header>