@extends('layout.master')

@section ('judul')

@endsection
@section ('content')

    <!-- slider section -->
    <section class="slider_section ">
        <div class="container ">
          <div class="row">
            <div class="col-lg-10 mx-auto">
              <div class="detail-box">
                <h1>
                  Temukan Restoran & Makanan
                </h1>
                <p>
                  Banyak restoran tersedia. Harga Kaki 5 Rasa Bintang 5.
                </p>
              </div>
             
            </div>
          </div>
        </div>
        <div class="slider_container">
          <div class="item">
            <div class="img-box">
              <img src="{{asset('admin/images/slider-img1.png')}}" alt="" />
            </div>
          </div>
          <div class="item">
            <div class="img-box">
              <img src="{{asset('admin/images/slider-img2.png')}}" alt="" />
            </div>
          </div>
          <div class="item">
            <div class="img-box">
              <img src="{{asset('admin/images/slider-img3.png')}}" alt="" />
            </div>
          </div>
          <div class="item">
            <div class="img-box">
              <img src="{{asset('admin/images/slider-img4.png')}}" alt="" />
            </div>
          </div>
          <div class="item">
            <div class="img-box">
              <img src="{{asset('admin/images/slider-img1.png')}}" alt="" />
            </div>
          </div>
          <div class="item">
            <div class="img-box">
              <img src="{{asset('admin/images/slider-img2.png')}}" alt="" />
            </div>
          </div>
          <div class="item">
            <div class="img-box">
              <img src="{{asset('admin/images/slider-img3.png')}}" alt="" />
            </div>
          </div>
          <div class="item">
            <div class="img-box">
              <img src="{{asset('admin/images/slider-img4.png')}}" alt="" />
            </div>
          </div>
        </div>
      </section>
      <!-- end slider section -->
    </div>
  
  
    <!-- recipe section -->
  
    @include('partial.resto')
    <!-- end recipe section -->
  
    <!-- app section -->
  
    
    <!-- end app section -->
  <br><br>
    <!-- about section -->
    <section class="about_section layout_padding">
      <div class="container">
        <div class="col-md-11 col-lg-10 mx-auto">
          <div class="heading_container heading_center">
            <h2>
              Tentang Kami
            </h2>
          </div>
          <br>
          <div class="box">
            
            <div class="detail-box">
              <p>
                Restorun merupakan sebuah platform promosi dan pencarian restoran serta makanan yang tersedia di sekitar Anda!
              </p>
              
            </div>
          </div>
        </div>
      </div>
    </section>
    
  
    <!-- end about section -->
  
    <!-- news section -->
  
   
    <!-- end news section -->
  
    <!-- client section -->
  
    
  
    <!-- end client section -->

@endsection