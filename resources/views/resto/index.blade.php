@extends('layout.mastera')

@section('judul')

@endsection

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<h1> Daftar Restoran </h1>
<br>

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Alamat</th>
                <th scope="col">Kontak</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
                @forelse ($resto as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->alamat}}</td>
                        <td>{{$value->kontak}}</td>
                        <td>
                            <a href="/resto/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/menu/{{$value->id}}" class="btn btn-success">Menu</a>
                            <a href="/resto/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/resto/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    
    <a href="/resto/create" class="btn btn-primary mb-3">Tambah Restoran</a>

    @endsection
    

