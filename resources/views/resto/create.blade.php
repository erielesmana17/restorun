@extends('layout.mastera')

@section('judul')
Tambah Resto
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endpush


@section('content')

<form action="/resto" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama </label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Alamat</label>
      <textarea type="text" name="alamat" class="form-control"></textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Kontak</label>
        <input type="number" name="kontak" class="form-control">
      </div>
      @error('kontak')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

    
    @endsection
    

