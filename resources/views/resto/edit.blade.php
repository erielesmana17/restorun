
@extends('layout.mastera')

@section('judul')
Detail  Resto
@endsection
@section('content')
<div>
    <h2>Update Resto {{$resto->id}}</h2>
    <form action="/resto/{{$resto->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Nama Resto</label>
            <input type="text" class="form-control" name="nama" value="{{$resto->nama}}">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Alamat</label>
            <input type="text" class="form-control" name="alamat"  value="{{$resto->alamat}}">
            @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Kontak</label>
            <input type="text" class="form-control" name="kontak"  value="{{$resto->kontak}}">
            @error('kontak')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>

    
    @endsection
    

